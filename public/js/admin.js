$(document).ready(function(){
	$(".ajax-delete").click(function(e){
		let uri='admin/danhmuc/'+$(this).data('id');
		if(!confirm("bạn đã chắc muốn xóa")){
			return false;
		}else{
		    e.preventDefault();
		    console.log(uri);
		    $.ajaxSetup({
			  headers: {
			    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			  }
			});
			$.ajax({
	        type: 'Delete',
	        url: uri,
	        success: function (data) {
	           $(".data-table").html(data.html);
	        },
	        error: function (data) {
	            console.log(data);
	        }
	   		 });
		}
		
	});
});