﻿LearningEnglishLuyenNgheTiengAnh-VOA-3100823.mp3

[00:00:05] From the O A learning English. This is in the news. 

[00:00:10] It has been a week since gunmen attacked this shopping center in Kenya's capital Nairobi. The attack left more than 70 people dead. The Kenyan Red Cross said Thursday that about 60 others were still missing. The attack left many people feeling powerless and shocked. But instead of hiding at home Kenyans have come out to donate food clothing their own blood and money. The country appears more united because of the attack. The violence at the Westgate shopping mall lasted four days. During that time medical workers offered help to victims and security forces at a nearby community center. Those seeking assistance often had to run for cover as gunshots and explosions were heard. A small army of volunteers came to the centre. They provided food medical services and emotional support to an endless line of people. VMO Shah helped to direct the community center operation. He says so many people wanted to feed Kenyan security forces or help the victims that he had to turn people away. Enzio Kar wai ta works for Safaricom in just two days. The communications company collected more than seven hundred thousand dollars for the Kenyan Red Cross. 

[00:02:03] This is the Kenya I know and I know one will build on their spirit of togetherness. So you know pleasantly surprised in how quickly the funds have come together but not entirely surprised that we've pulled together from the various communities religious affiliations. 

[00:02:32] He also says there has been huge support from the many Kenyans who live overseas but the mall attack has also incited some tension in Kenya. The Somali Islamist group Al Shabab claimed responsibility for the attack. Some reports say the attackers had freed some Muslims but executed other people after those stories appeared. There was criticism of Muslims and Somalis on the social networking websites. There are still tribal tensions left from the Kenyan elections earlier this year VMOs Shah says. The crisis last weekend brought the nation together but he hopes that Kenyans do not start fearing one another after seeing such violence. 

[00:03:33] This is pure terrorism. This is not about religion. This is not about one religion against religion. And I think Kenyans stand together. We've got all different tribes coming together all different people coming together and saying united we stand divided we fall. 

[00:03:49] Nyama Amir collects money for St John Ambulance Kenya a medical aid group. She says the mall attack shows that Kenyans have closed the wounds from the country's 2007 elections. 

[00:04:06] People have vitiates peace have learnt to appreciate each other and the diversity within ourselves is a strength and not a mind to divide the country. 

[00:04:18] Najma Amir is a Muslim. She describes the attack as senseless violence that has nothing to do with Islam. On Tuesday Kenyan President Uhuru Kenyatta also appealed for unity. He declared that Kenya would defeat the monster of terrorism one that wanted to tear the country apart and violence in the news from the AU a learning English. 

[00:04:49] I'm Steve. 

