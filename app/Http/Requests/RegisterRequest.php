<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:5|max:60',
            'email' => 'required|email',
            'password' => 'required|min:6'
            
        ];
    }
    public function messages(){
        return [
            'name.required' => 'bạn không được để trống username',
            'name.min' => 'tên bạn nhập phải tối thiểu 4 kí tự',
            'name.max' => 'password bạn nhập phải tối đa 60 kí tự ',
            'email.required' => 'bạn không được để tróng email',
            'email.email' => 'email bạn nhập không đúng định dạng ',
            'password.required' => 'bạn không được để trống password',
            'password.min' => 'password bạn nhập phải tối thiểu 6 kí tự' 
        ];
    }
}
