<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Conversation;
use App\Events\SendMessageEvent;
use Session;
use App\Permission;
class ConversationController extends Controller
{
    public function getConversation($id){
    	 $conversation = DB::table('permission')
    	 	->join('conversation','conversation.id_permission','permission.id')
    	 	->join('users','users.id','permission.id_user')
    	 	->select('users.name as username','conversation.content as content','conversation.time as time','conversation.id as id','permission.permission as permission')
    	 	->where('permission.id_report',$id)
    	 	->orderBy('time','asc')
    	 	->get();
    	 $id_user = Session::get('user')->id;
    	 $permission = Permission::where([['id_report',$id],['id_user',$id_user]])->get();
    	 $per_user = $permission[0]->permission;
    	 return view('conversation.list',compact('conversation','per_user'));
    }
    public function postConversation(Request $request){
    	 if($request->ajax()){
    	 	$id_cover = $request->id;
    	 	$new_content = $request->content;
            $update_conver = Conversation::find($id_cover);
            $update_conver->content = $new_content;
    	 	$update_conver->save();
    	 	event(new SendMessageEvent($update_conver));
    	 	return ;
    	 }
    }
}
