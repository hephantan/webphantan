<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DanhMuc;
use Session;

class DanhMucController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $list_dm=DanhMuc::all();
        return view("admin.danhmuc.list",['dm'=>$list_dm]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.danhmuc.add");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new=$request->all();
        $dm=new DanhMuc();
        $dm->create($new);
        return redirect("admin/danhmuc/create")->with("message","thêm mới thành công!");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dm=DanhMuc::find($id);
        return view("admin.danhmuc.update",['dm'=>$dm]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $dm=DanhMuc::find($id);
       $new=$request->all();
       $dm->update($new);
       Session::flash('message', 'Cập nhật thành công!');
       return redirect("admin/danhmuc/$id/edit");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
    	$dm=DanhMuc::find($id);
    	$dm->delete();
    	Session::flash("message","xóa thành công");
    	return redirect("admin/danhmuc");
    }

}
