<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $table='conversation';
    protected $fillable=[
    	'id_permission',
    	'time',
    	'content'
    ];
    public function permission(){
    	return $this->belongsTo('App\Permission','id_permission','id');
    }
}
