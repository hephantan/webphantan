<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table='history';
    protected $table=[
    	'id_permission',
    	'content_before',
    	'content_after',
    	'content_edit',
    	'time',
    	'date'
    ];
}
