@extends("layout.index")
@section("content")
 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Report
                            <small>List</small>
                        </h1>
                    </div>
                    <div class="col-lg-12">
                        @if(Session::has('message'))
                            <br/>
                            <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-12 data-table">
                        @include("report.table")
                    </div>
                    <div class="col-lg-1 col-lg-offset-10">
                        <a href="report/create"><button type="button" class="btn btn-primary"><i class="fa fa-plus-circle"></i>Add new</button></a>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>   
@endsection
