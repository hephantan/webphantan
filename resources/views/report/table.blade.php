<table class="table table-striped table-bordered table-hover" id="dataTables-example">
	                        <thead>
	                            <tr align="center">
	                                <th>STT</th>
	                                <th>Report name</th>
	                                <th>Description</th>
	                                <th>Role</th>
	                                <th>Decentralization</th>
	                                <th>Delete</th>
	                                <th>Update</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                        	@foreach($report as $key => $val)
	                            <tr class="odd gradeX" align="center">
	                                <td>{{$key+1}}</td>
	                                <td><a href="conversation/{{$val->id}}">{{$val->name}}</a></td>
	                                <td>{!!$val->content!!}</td>
	                                <td>
	                                	@if($val->permission == 0)
	                                		Read
	                                	@elseif($val->permission == 1)
											R & W
	                                	@else
											Admin
	                                	@endif
	                                </td>
	                                <td>
	                                	@if($val->permission == 2)
	                                		<a href=""><button type="button" class="btn btn-primary">Decentralization</button></a>
	                                	@endif
	                                </td>
	                                <td>
	                                	@if($val->permission == 2)
	                                	<form action="report/{{$val->id}}" method="POST">
									    	<input type="hidden" name="_method" value="DELETE">
									    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
									   		<button type="submit" class="btn btn-primary xoa"><i class="fa fa-trash"></i> Delete</button>
										</form>
										@endif
	                                </td>
	                                  
	                                <td class="center">
	                                	@if($val->permission == 2)
	                                	<a href='report/{{$val->id}}/edit'><button type="button" class="btn btn-primary">
	                                	<i class="fa fa-pencil"></i> Update
	                                	</button></a>
	                                	@endif
	                                </td>
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>

<script type="text/javascript">
	$(".xoa").click(function(event){
			if(!confirm("bạn có chắc chắn muốn xóa không")){
				return false;
			}

	});
</script>
	                    