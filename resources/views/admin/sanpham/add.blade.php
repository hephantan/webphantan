@extends("admin.layout.index")
@section("content")
 <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Danh Mục
                            <small>Thêm mới</small>
                        </h1>
                    </div>
                    <div class="col-lg-12">
                        @if(Session::has('message'))
                            <br/>
                            <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                         {!! Form::open( ['url' => "admin/sanpham/", 'method' => 'POST', 'class' => 'form-horizontal', 'name'=>'uploadform', 'files'=>true] ) !!}
                             <div class="form-group">
                                <label >Danh mục </label>
                                <div>
                                     {!! Form::select('id_dm',$danhmuc,'', array('class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Tên sản phẩm</label>
                                {!! Form::text('ten_sp',old('ten_sp',''), array('class' => 'form-control')) !!}
                            </div>
                             <div class="form-group">
                                <label>Giá sản phẩm</label>
                                {!! Form::text('gia_sp',old('gia_sp',''), array('class' => 'form-control')) !!}
                            </div>
                             <div class="form-group">
                                <label>Số lượng</label>
                                {!! Form::text('so_luong',old('so_luong',''), array('class' => 'form-control')) !!}
                            </div>
                             <div class="form-group">
                                <label>Kích thước</label>
                                {!! Form::text('kich_thuoc',old('kich_thuoc',''), array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label>Ảnh</label>
                                 {!!Form::file('anh')!!} 
                            </div>
                            <div class="form-group">
                                <label>Trọng lượng</label>
                                {!! Form::text('trong_luong',old('trong_luong',''), array('class' => 'form-control')) !!}
                            </div>
                             <div class="form-group">
                                <label>Màu sắc</label>
                                {!! Form::text('mau_sac',old('mau_sac',''), array('class' => 'form-control')) !!}
                            </div>
                             <div class="form-group">
                                <label>Âm thanh</label>
                                {!! Form::text('am_thanh',old('am_thanh',''), array('class' => 'form-control')) !!}
                            </div>
                             <div class="form-group">
                                <label>Bộ nhớ</label>
                                {!! Form::text('bo_nho',old('bo_nho',''), array('class' => 'form-control')) !!}
                            </div>
                             <div class="form-group">
                                <label>Hệ điều hành</label>
                                {!! Form::text('he_dieu_hanh',old('he_dieu_hanh',''), array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label>Thẻ nhớ</label>
                                {!! Form::text('the_nho',old('the_nho',''), array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label>Camera</label>
                                {!! Form::text('camera',old('camera',''), array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label>Pin</label>
                                {!! Form::text('pin',old('pin',''), array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label>Bảo hành</label>
                                {!! Form::text('bao_hanh',old('bao_hanh',''), array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label>Kết nối</label>
                                {!! Form::text('ket_noi',old('ket_noi',''), array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label>Giá khuyến mãi</label>
                                {!! Form::text('gia_km',old('gia_km',''), array('class' => 'form-control')) !!}
                            </div>
                            <div class="form-group">
                                <label>Bắt đâu khuyến mãi</label>
                                {!! Form::date('batdau_km', \Carbon\Carbon::now(),array('class' => 'form-control'))!!}
                            </div>
                            <div class="form-group">
                                <label>Kết thúc khuyến mãi</label>
                                {!! Form::date('ketthuc_km', \Carbon\Carbon::now(),array('class' => 'form-control')) !!}
                            </div>
                            
                            <button type="submit" class="btn btn-primary">Thêm</button>
                            <button type="reset" class="btn btn-primary">Làm mới</button>
                       {!! Form::close() !!}
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>

@endsection