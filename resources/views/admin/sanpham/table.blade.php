 <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Tên sản phẩm</th>
                                <th>Danh mục</th>
                                <th>Ảnh mô tả </th>
                                <th>Giá</th>
                                <th>Số lượng</th>
                                <th>Sửa</th>
                                <th>Xóa</th>
                            </tr>
                        </thead>
                        <tbody>
                        	@foreach($sp as $key => $val)
                           <tr class="odd gradeX" align="center">
	                                <td>{{$key+1}}</td>
	                                <td>{{$val->ten_sp}}</td>
	                                <td>{{$val->danhMuc->ten_dm}}</td>
	                                <td>
	                                 @if($val->anh_sp)
                   					 <img src="<?php echo URL::to('upload/sanpham/'.$val->anh_sp);?>" class="thumbnail" style="width: 50px; margin: 0px">
              						 @endif
	                                </td>
	                                <td>{{$val->gia_sp}}</td>
	                                <td>{{$val->so_luong}}</td>
	                                <td>
	                                	<!-- <form action="admin/sanpham/{{$val->id}}" method="POST">
									    <input type="hidden" name="_method" value="DELETE">
									    <input type="hidden" name="_token" value="{{ csrf_token() }}">
									     <button type="submit" class="btn btn-primary"><i class="fa fa-trash"></i> Xóa</button>
										</form> -->
										<button type="button" class="btn btn-primary deletee">Xóa</button>
	                                </td>
	                                <td class="center"><a href='{{URL::to("admin/sanpham/$val->id"."/edit")}}'><button type="button" class="btn btn-primary">
	                                <i class="fa fa-pencil"></i> Sửa
	                                </button></a></td>
	                            </tr> 
	                           @endforeach              
                        </tbody>
                    </table>

<script type="text/javascript">
	$('.deletee').click(function(){
		if(!confirm('bạn có chắc muốn xóa')){
			return;
		}
		$.ajaxSetup({
  			headers: {
    		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  			}
			});
		$.ajax({
			url: 'admin/sanpham/1',
			type: 'delete',
			success: function(data){
				$('.deleteee').remove();
				$('.deleteee').html(data.html);
			}

		});
	});
</script>         