@extends("admin.layout.index")
@section("content")
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Sản Phẩm
                            <small>Danh sách </small>
                        </h1>
                    </div>
                    <div class="col-lg-12">
                        @if(Session::has('message'))
                            <br/>
                            <p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
                        @endif
                    </div>
                    <!-- /.col-lg-12 -->  
                    <div class="deleteee">
                         @include('admin.sanpham.table')
                    </div>               
                    <div class="col-lg-offset-10 col-lg-1">
                    	<a href="admin/sanpham/create" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Thêm </a>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
@endsection