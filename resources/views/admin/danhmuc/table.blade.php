<table class="table table-striped table-bordered table-hover" id="dataTables-example">
	                        <thead>
	                            <tr align="center">
	                                <th>STT</th>
	                                <th>Tên danh mục</th>
	                                <th>Mô tả</th>
	                                <th>Xóa</th>
	                                <th>Sửa</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                        	@foreach($dm as $key => $val)
	                            <tr class="odd gradeX" align="center">
	                                <td>{{$key+1}}</td>
	                                <td>{{$val->ten_dm}}</td>
	                                <td>{!!$val->noidung!!}</td>
	                                <td>
	                                	<form action="admin/danhmuc/{{$val->id}}" method="POST">
									    <input type="hidden" name="_method" value="DELETE">
									    <input type="hidden" name="_token" value="{{ csrf_token() }}">
									   <button type="submit" class="btn btn-primary"><i class="fa fa-trash"></i> Xóa</button>
									</form
	                                </td>
	                                <td class="center"><a href='{{URL::to("admin/danhmuc/$val->id"."/edit")}}'><button type="button" class="btn btn-primary">
	                                <i class="fa fa-pencil"></i> Sửa
	                                </button></a></td>
	                            </tr>
	                            @endforeach
	                        </tbody>
	                    </table>


	                    