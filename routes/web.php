<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('getlogin','LoginController@getLogin');
Route::post('postlogin','LoginController@postLogin');

Route::get('getlogout',function(){
	if(Session::has('user')){
		Session::flush();
		return redirect('getlogin');
	}
});

Route::group(['middleware' => 'member'],function(){
	Route::resource('report','ReportController');
	Route::get('permission','PermissionController@getPermission');
	Route::post('permission','PermissionController@postPermission');
	Route::get('conversation/{id}','ConversationController@getConversation');
	Route::post('conversation','ConversationController@postConversation');
	Route::get('information/{id}','InformationController@getInformation');
	Route::post('information','InformationController@postInformation');
	Route::Resource('template','TemplateController');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
